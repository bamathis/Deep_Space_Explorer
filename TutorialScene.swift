//
//  TutorialScene.swift
//  Deep Space Explorer
//
//  Created by Brandon Mathis on 8/27/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class TutorialScene: SKScene
    {
    var text:SKLabelNode!
    var returnButton: SKLabelNode!
    var previousButton: SKLabelNode!
    var controller: GameViewController!
    var yPos = CGFloat(0)
    
    override func didMove(to view: SKView)
        {
        
        }
//==============================================================================
//Prepares the display of the tutorial scene
    func setupDisplay(to view: SKView)
        {
        let backgroundLabel = SKShapeNode(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: frame.midX * 2, height: frame.midY * 2)))
        backgroundLabel.fillColor = UIColor(colorLiteralRed: 30.0/255.0, green: 0/255.0, blue: 30.0/255.0, alpha: 1.0)
        addChild(backgroundLabel)
        let backgroundPicture = SKSpriteNode(imageNamed: "black-and-white-rocket-800px.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX, height: frame.maxY))
        backgroundPicture.alpha = 0.1
        addChild(backgroundPicture)
        
        let tutorialLabel = SKLabelNode()
        tutorialLabel.text = "Tutorial"
        tutorialLabel.fontSize = CGFloat(frame.midY/5)
        tutorialLabel.fontName = "AvenirNext-Bold"
        tutorialLabel.fontColor = UIColor.white
        tutorialLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        tutorialLabel.position = CGPoint(x: frame.midX, y: frame.maxY)
        addChild(tutorialLabel)
        var yPos = frame.midY + (frame.midY/5)
        
        text = SKLabelNode()
        text.text = "Tap the screen to start the rocket."
        text.fontSize = CGFloat(frame.midY/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.white
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
        
        yPos = yPos - CGFloat(Double(frame.midY)/8)
        text = SKLabelNode()
        text.text = "After it has started moving, tap again to"
        text.fontSize = CGFloat(frame.midY/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.white
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
        
        yPos = yPos - CGFloat(Double(frame.midY)/8)
        text = SKLabelNode()
        text.text = "switch the vertical direction of the rocket"
        text.fontSize = CGFloat(frame.midY/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.white
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
        
        yPos = yPos - CGFloat(Double(frame.midY)/8)
        text = SKLabelNode()
        text.text = "to avoid those pesky asteroids."
        text.fontSize = CGFloat(frame.midY/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.white
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
        
        yPos = yPos - CGFloat(Double(frame.midY)/8)
        text = SKLabelNode()
        text.text = "How far can you get?"
        text.fontSize = CGFloat(frame.midY/8)
        text.fontName = "AvenirNext-Bold"
        text.fontColor = UIColor.white
        text.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        text.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        text.position = CGPoint(x: frame.midX, y: yPos)
        addChild(text)
        
        let returnToMenuLabel = SKLabelNode(text:"Tap to go to menu")
        returnToMenuLabel.alpha = 0.35
        returnToMenuLabel.fontSize = CGFloat(frame.midY/6)
        returnToMenuLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        returnToMenuLabel.fontColor = UIColor.white
        returnToMenuLabel.position = CGPoint(x: frame.midX, y: 0)
        addChild(returnToMenuLabel)
        }
//==============================================================================
//Returns to the previous screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        controller.showMenu()
        }
    }
