# Deep_Space_Explorer

[Deep_Space_Explorer](https://gitlab.com/bamathis/Deep_Space_Explorer) is a tap-based space exploration iOS game developed using Swift.

### Dependencies

- Swift 3.1+
- Google Mobile Ads SDK 7.31.0

### Download On the App Store
https://itunes.apple.com/us/app/deep-space-explorer/id1276091882?mt=8
