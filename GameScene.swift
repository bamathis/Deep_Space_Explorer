//
//  GameScene.swift
//  Deep Space Explorer
//
//  Created by Brandon Mathis on 8/11/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene
    {
    var center = CGPoint(x: 0, y: 0)
    var score = 0
    var gameOver = false
    var scoreLabel = SKLabelNode()
    var ground = SKShapeNode()
    var ceiling = SKShapeNode()
    var menuLabel: SKLabelNode!
    var width = CGFloat(0)
    var height = CGFloat(0)
    var rocket = Rocket(imageNamed: "black-and-white-rocket-800px.png")
    var xDelta: Double!
    var yDelta: Double!
    var animating = false
    var timer = Timer()
    var asteroids = [SKSpriteNode]()
    var count = 0
    var playableSize = CGFloat(0)
    var controller: GameViewController!
    var speedUp = false
    var maxSpeed: Double!

//================================================================
//Sets the initial display for the game
    override func didMove(to view: SKView)
        {
        center = CGPoint(x: view.center.x, y: view.center.y)
        width = center.x * 2
        height = center.y * 2
        backgroundColor = UIColor(colorLiteralRed: 30.0/255.0, green: 0/255.0, blue: 30.0/255.0, alpha: 1.0)
        ground  = SKShapeNode(rectOf: CGSize(width: width * 3, height: height/7))
        ground.fillColor = UIColor.darkGray
        ground.strokeColor = UIColor.black
        addChild(ground)
            
        ceiling  = SKShapeNode(rectOf: CGSize(width: width * 3, height: height/7))
        ceiling.position = CGPoint(x: CGFloat(0), y: height)
        ceiling.fillColor = UIColor.darkGray
        ceiling.strokeColor = UIColor.black
        addChild(ceiling)
        scoreLabel.position = CGPoint(x: 0, y: height)
        scoreLabel.fontColor = UIColor.white
        scoreLabel.fontSize = 20
        scoreLabel.text = "Score: 0"
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        scoreLabel.fontName = "AvenirNext-Bold"
        addChild(scoreLabel)
            
        rocket.setDiameter(circleOfRadius: width/50)
        rocket.xPos = Double(width/17)
        rocket.yPos = Double(height/2)
        rocket.draw()

        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: rocket.size.width/2, y: 0))
        bezierPath.addLine(to: CGPoint(x: 0, y:  -rocket.size.height/2))
        bezierPath.addLine(to: CGPoint(x: -rocket.size.width/2, y:  -rocket.size.height/2))   
        bezierPath.addLine(to: CGPoint(x: -rocket.size.width/2, y: rocket.size.height/2))
        bezierPath.addLine(to: CGPoint(x: 0, y: rocket.size.height/2))
        bezierPath.close()
        rocket.physicsBody = SKPhysicsBody(polygonFrom: bezierPath.cgPath)
	rocket.physicsBody?.affectedByGravity = false
        rocket.physicsBody?.isDynamic = true
        addChild(rocket)
        playableSize = height - height/7 - height/7
        drawAsteroids()
        }
//=======================================================================
//Handles user interaction with the game
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if(gameOver)
            {
            if let touch = touches.first
                {
                let pos = touch.location(in: self)
                let node = self.atPoint(pos)
                if (node == menuLabel)
                    {
                    if(controller.interstitial.isReady && !controller.adsDisabled)
                        {
                        controller.interstitial.present(fromRootViewController: controller)
                        }
                    controller.showMenu()
                    }
                }
            }
        else if(!animating)
            {
            moveRocket()
            }
        else
            {
            swapGravity()
            }
        }
//===================================================================
//Adjusts the position of the rocket
    func moveRocket()
        {
        if #available(iOS 10.0, *)
            {
            yDelta = Double(height) / 100.0
            xDelta = Double(height) * Double(height) * Double(height) / (Double(width) * Double(width) * 15)
            if(width <= 668 || width > 850)
                {
                xDelta = xDelta / 4.0
                yDelta = yDelta / 4.0
                maxSpeed = xDelta * 1.5
                }
            else 
                {
                xDelta = xDelta / 2.0
                yDelta = yDelta / 2.0
                maxSpeed = xDelta * 1.5
                }

            rocket.adjustPosition(xAccel: xDelta, yAccel: yDelta)
            animating = true
            timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true, block: updateScore)
            }
        else
            {
                // Fallback on earlier versions
            }
        }
//====================================================================
//Changes the direction the rocket is moving
    func swapGravity()
        {
        rocket.direction = rocket.direction * -1
        }
//====================================================================
//Updates the score and physics for the game
    func updateScore(time: Timer)
        {
        scoreLabel.text = "Score: " + String(score)
        if(getCurrentScore() == -1)
                {
                timer.invalidate()
                rocket.gameOver = true
                gameOver = true
                gameEnded()
                }
        if(score % 10 == 0 && speedUp)
            {
            if(xDelta < maxSpeed)
                {
                xDelta = xDelta * 1.05
                yDelta = yDelta * 1.05
                }
            else
                {
                xDelta = xDelta * 1.005
                yDelta = yDelta * 1.005
                }

            rocket.yMotion = yDelta
            speedUp = false
            count = 0
            }
        else if(score % 10 == 0)
            {
            
            }
        else
            {
            speedUp = true
            }
        moveAsteroids()
        rocket.draw()
        count = count + 1
        }
//===============================================================================================================
//Creates the asteroids to be used in the game
    func drawAsteroids()
        {

        for i in 0 ... 11
            {
            let asteroid = SKSpriteNode(imageNamed: "asteroid.png")
            asteroid.size = CGSize(width: width/25, height: width/25)
            asteroid.physicsBody = SKPhysicsBody(circleOfRadius: CGFloat(asteroid.size.width/2 - 5))
            asteroid.physicsBody?.affectedByGravity = false
            asteroid.physicsBody?.isDynamic = false
            if(i % 3 == 0)
                {
                asteroid.position = CGPoint(x: width + CGFloat(i/3) * width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7)
                }
            else if (i % 3 == 1)
                {
                asteroid.position = CGPoint(x: width + CGFloat(i/3) * width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7 + playableSize / 3)
                }
            else
                {
                asteroid.position = CGPoint(x: width + CGFloat(i/3) * width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7 + playableSize / 3 * 2)
                }
            asteroids.append(asteroid)
            addChild(asteroid)
            }
        }
//===============================================================================================================
//Adjusts the position of the asteroids in the game
    func moveAsteroids()
        {
        var i = 0
        for asteroid in asteroids
            {
            if(asteroid.position.x < 0)			//if the asteroids are off the screen
                {
                if(i % 3 == 0)
                    {
                    asteroid.position = CGPoint(x: width + width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7)
                    score = score + 1
                    }
                else if(i % 3 == 1)
                    {
                    asteroid.position = CGPoint(x: width + width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7 + playableSize / 3)
                    }
                else
                    {
                    asteroid.position = CGPoint(x: width + width/3, y: CGFloat(arc4random_uniform(UInt32(playableSize)) / 3) + height / 7 + playableSize / 3 * 2)
                    }
                }
             else
                {
                asteroid.position = CGPoint(x: asteroid.position.x - CGFloat(xDelta), y:asteroid.position.y)
                }
            i = i + 1
            }
        }
//===============================================================================================================
//Returns whether the game is still being played
    func getCurrentScore() -> Int
        {
        var calculatedScore = 0
        if( rocket.physicsBody?.allContactedBodies().count != 0)
            {
            calculatedScore = -1
            }
        if(calculatedScore != -1)
            {
            calculatedScore = 1
            }
        if(ground.position.y + 40 > CGFloat(rocket.yPos))
            {
            rocket.yPos = rocket.yPos + 5
            rocket.direction = rocket.direction * -1
            }
        else if(ceiling.position.y - 40 < CGFloat(rocket.yPos))
            {
            rocket.yPos = rocket.yPos - 5
            rocket.direction = rocket.direction * -1
            }
        return calculatedScore
        }
//===============================================================================================================
//Displays a game over screen
    func gameEnded()
        {
        if(score > controller.highscore)
            {
            controller.highscore = score
            controller.userDefaults.setValue(controller.highscore, forKey: "highscore")
            }
        let backgroundLabel = SKShapeNode(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: center.x * 2, height: center.y * 2)))
        backgroundLabel.fillColor = UIColor(colorLiteralRed: 30.0/255.0, green: 0/255.0, blue: 30.0/255.0, alpha: 1.0)
        addChild(backgroundLabel)
        let backgroundPicture = SKSpriteNode(imageNamed: "black-and-white-rocket-800px.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX, height: frame.maxY))
        backgroundPicture.alpha = 0.1
        addChild(backgroundPicture)
        menuLabel = SKLabelNode(text:"Tap to return to menu")
        menuLabel.alpha = 0.35
        menuLabel.fontSize = CGFloat(frame.midY/6)
        menuLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        menuLabel.fontColor = UIColor.white
        menuLabel.position = CGPoint(x: center.x, y: 0)
        addChild(menuLabel)
        let nameLabel = SKLabelNode(text:"Deep Space Explorer")
        nameLabel.fontName = "AvenirNext-Bold"
        nameLabel.fontSize = CGFloat(frame.midY/3.5)
        nameLabel.fontColor = UIColor.white
        nameLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        nameLabel.position = CGPoint(x: center.x, y: center.y * 2)
        addChild(nameLabel)
        let nameLabel2 = SKLabelNode(text:"Explorer")
        nameLabel2.fontName = "AvenirNext-Bold"
        nameLabel2.fontSize = CGFloat(frame.midY/3.5)
        nameLabel2.fontColor = UIColor.white
        nameLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        nameLabel2.position = CGPoint(x: center.x, y: nameLabel.position.y - (frame.midY/2))
        //addChild(nameLabel2)
        let gameOverLabel = SKLabelNode(text:"Score: " + String(score))
        gameOverLabel.fontSize = CGFloat(frame.midY/4)
        gameOverLabel.fontColor = UIColor.white
        gameOverLabel.fontName = "AvenirNext-Bold"
        gameOverLabel.position = CGPoint(x: center.x, y: center.y)
        addChild(gameOverLabel)
        let highScoreLabel = SKLabelNode(text:"High score: " + String(controller.highscore))
        highScoreLabel.fontSize = CGFloat(frame.midY/6)
        highScoreLabel.fontColor = UIColor.white
        highScoreLabel.position = CGPoint(x: center.x, y: gameOverLabel.position.y - 50)
        addChild(highScoreLabel)
        }
    }
