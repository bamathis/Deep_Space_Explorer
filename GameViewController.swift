//
//  GameViewController.swift
//  Deep Space Explorer
//
//  Created by Brandon Mathis on 8/11/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import StoreKit
import GoogleMobileAds


class GameViewController: UIViewController, SKPaymentTransactionObserver, SKProductsRequestDelegate, GADInterstitialDelegate
    {
    var gameScene: GameScene!
    var menuScene: MenuScene!
    var tutorialScene: TutorialScene!
    var highscore = 0
    var userDefaults = UserDefaults.standard
    var startX = CGFloat(0.0)
    var startY = CGFloat(0.0)
    var adsDisabled = false
    var interstitial: GADInterstitial!
    var appId = ""
    var unitId = ""
    var productID:NSSet!
    var productsRequest:SKProductsRequest!
    var productsFromStore = [SKProduct]()
//=====================================================================
//Sets up Google Mobile Ads and loads initial screen
    override func viewDidLoad()
        {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        GADMobileAds.configure(withApplicationID: appId)
        createAndLoadInterstitial()
        if(SKPaymentQueue.canMakePayments())
            {
            productID = NSSet(objects: "RemoveSpaceAds")
            productsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            productsRequest.delegate = self
            productsRequest.start()
            SKPaymentQueue.default().add(self)
            }
        if (userDefaults.value(forKey: "adsDisabled") != nil)
            {
            adsDisabled = userDefaults.value(forKey: "adsDisabled") as! Bool
            }
        if (userDefaults.value(forKey: "highscore") != nil)
            {
            highscore = userDefaults.value(forKey: "highscore") as! Int
            showMenu()
            }
        else
            {
            highscore = -1
            showTutorial()
            }
        }
//=====================================================================
//Forces the screen to rotate
    override var shouldAutorotate: Bool
        {
        return true
        }
//=====================================================================
//Forces device orientation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
        {
        if UIDevice.current.userInterfaceIdiom == .phone
            {
            return .allButUpsideDown
            }
        else
            {
            return .all
            }
        }
//=====================================================================
    override func didReceiveMemoryWarning()
        {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
        }
//=====================================================================
//Hides the Status Bar
    override var prefersStatusBarHidden: Bool
        {
        return true
        }
//=======================================================================
//Creates a Google Mobile Ad request
    func createAndLoadInterstitial()
        {
        interstitial = GADInterstitial(adUnitID: unitId)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        }
//=======================================================================
//Calls createAndLoadInterstitial to create a Google Mobile Ad request
    func interstitialDidDismissScreen(_ ad: GADInterstitial)
        {
        createAndLoadInterstitial()
        }
//=====================================================================
//Presents the Menu Screen
    func showMenu()
        {
        let skView = view as! SKView
        menuScene = MenuScene(size: skView.bounds.size)
        menuScene.controller = self
        menuScene.scaleMode = .aspectFill
        skView.presentScene(menuScene)
        }
//======================================================================
//Presents the Tutorial Screen
    func showTutorial()
        {
        let skView = view as! SKView
        tutorialScene = TutorialScene(size: skView.bounds.size)
        tutorialScene.controller = self
        tutorialScene.scaleMode = .aspectFill
        tutorialScene.setupDisplay(to: skView)
        skView.presentScene(tutorialScene)
        }
//======================================================================
//Presents the Game Screen
    func startGame()
        {
        let skView = self.view as! SKView
        gameScene = GameScene(size: skView.bounds.size)
        gameScene.scaleMode = .aspectFill
        gameScene.controller = self
        skView.presentScene(gameScene)
        }
//=======================================================================
//Makes a list of products that were requested for purchase
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
        {
        let products = response.products
        for product in products
            {
            productsFromStore.append(product)
            }
        }
//=======================================================================
//Purchases the product that was passed if payment is enabled
    func purchaseProduct(product: SKProduct)
        {
        if (SKPaymentQueue.canMakePayments())
            {
            SKPaymentQueue.default().add(SKPayment(product: product))
            }
        else
            {
            print("Payment disabled")
            }
        }
//======================================================================
//Restores previously purchased items
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue)
        {
        for transaction in queue.transactions
            {
            let prodID = transaction.payment.productIdentifier as String
            switch prodID
                {
                case "RemoveSpaceAds":
                    adsDisabled = true
                    userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                    break
                default:
                    print("Item not found")
                }
            }
        }
//======================================================================
//Completes purchase of the requested products
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])
        {
        for transaction in transactions
            {
            switch transaction.transactionState
                {
                case .purchased:
                    let prodID = transaction.payment.productIdentifier as String
                    switch prodID
                        {
                        case "RemoveSpaceAds":
                            adsDisabled = true
                            userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                            break
                        default:
                            print("Item not found")
                        }
                    queue.finishTransaction(transaction)
                    break
                case .restored:
                    let prodID = transaction.payment.productIdentifier as String
                    switch prodID
                        {
                        case "RemoveSpaceAds":
                            adsDisabled = true
                            userDefaults.setValue(adsDisabled, forKey: "adsDisabled")
                            break
                        default:
                            break
                        }
                    queue.finishTransaction(transaction)
                    break
                case .failed:
                    queue.finishTransaction(transaction)
                    break
                default:
                    break
                }
            }
        }
    }
