//
//  Rocket.swift
//  Deep Space Explorer
//
//  Created by Brandon Mathis on 8/27/17..
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import Foundation
import SpriteKit

class Rocket: SKSpriteNode
{
    var xPos = 0.0
    var yPos = 0.0
    var diameter: CGFloat = 0.0
    var xMotion = 0.0
    var yMotion = 0.0
    var gameOver = false
    var direction = 1.0
    
//================================================================================
//Adjusts the speed and direction of the rocket
    func adjustPosition(xAccel: Double, yAccel: Double)
        {
        xMotion = xAccel
        yMotion = yAccel
        }
//===============================================================================
//Sets the size of the rocket
    func setDiameter(circleOfRadius: CGFloat)
        {
        diameter = circleOfRadius * 3
        size = CGSize(width: diameter * 1.5, height: diameter)
        }
//===============================================================================
//Sets the new rocket position
    func draw()
        {
        if(!gameOver)
            {
            yPos = yPos + (yMotion * direction)
            position = CGPoint(x:xPos, y: yPos)
            }
        }
}
