//
//  MenuScene.swift
// Deep Space Explorer
//
//  Created by Brandon Mathis on 8/27/17.
//  Copyright © 2017 Brandon Mathis. All rights reserved.
//

import SpriteKit
import StoreKit

class MenuScene: SKScene
    {
    var playButton: SKLabelNode!
    var tutorialButton: SKLabelNode!
    var purchaseButton: SKLabelNode!
    var controller: GameViewController!

//===============================================================================================================
//Sets up the menu screen
    override func didMove(to view: SKView)
        {
        backgroundColor = UIColor(colorLiteralRed: 30.0/255.0, green: 0/255.0, blue: 30.0/255.0, alpha: 1.0)
        let backgroundPicture = SKSpriteNode(imageNamed: "black-and-white-rocket-800px.png")
        backgroundPicture.position = CGPoint(x: frame.midX, y: frame.midY)
        backgroundPicture.scale(to: CGSize(width: frame.maxX, height: frame.maxY))
        backgroundPicture.alpha = 0.1
        addChild(backgroundPicture)
        playButton = SKLabelNode()
        playButton.text = "Play"
        playButton.fontSize = CGFloat(frame.midY/4)
        playButton.fontName = "AvenirNext-Bold"
        playButton.fontColor = UIColor.white
        playButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.65)
        addChild(playButton)
        tutorialButton = SKLabelNode()
        tutorialButton.text = "Tutorial"
        tutorialButton.fontColor = UIColor.white
        tutorialButton.fontSize = CGFloat(frame.midY/6)
        tutorialButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.45)
        tutorialButton.fontName = "AvenirNext-Bold"
        addChild(tutorialButton)
        purchaseButton = SKLabelNode()
        purchaseButton.text = "Remove Ads"
        if(controller.adsDisabled)
            {
            purchaseButton.fontColor = UIColor.lightGray
            }
        else
            {
            purchaseButton.fontColor = UIColor.white
            }
        purchaseButton.fontSize = CGFloat(frame.midY/6)
        purchaseButton.fontName = "AvenirNext-Bold"
        purchaseButton.position = CGPoint(x: frame.midX, y: frame.maxY * 0.25)
        addChild(purchaseButton)
        }
//===============================================================================================================  
//Handles user interaction
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
        {
        if let touch = touches.first
            {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            if (node == playButton)
                {
                controller.startGame()
                }
            else if (node == purchaseButton)
                {
                SKPaymentQueue.default().restoreCompletedTransactions()
                for product in controller.productsFromStore
                    {
                    let prodId = product.productIdentifier
                    if(prodId == "RemoveAds" && !controller.adsDisabled)
                        {
                        controller.purchaseProduct(product: product)
                        }
                    }
                }
            else if(node == tutorialButton)
                {
                controller.showTutorial()
                }
            }
        }
    }
